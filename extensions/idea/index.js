const { init } = require('./init');
const { dependency } = require('./dependency');

module.exports = {
    init,
    dependency
};