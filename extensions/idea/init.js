const fs = require('fs');
const path = require('path');
const createFileTree = require('create-file-tree');

const misc = 
`<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="ProjectRootManager" version="2" default="false" project-jdk-type="JavaSDK">
    <output url="file://$PROJECT_DIR$/out" />
  </component>
</project>`;

const modules = (projectName) => 
(`<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="ProjectModuleManager">
    <modules>
      <module fileurl="file://$PROJECT_DIR$/.idea/${projectName}.iml" filepath="$PROJECT_DIR$/.idea/${projectName}.iml" />
    </modules>
  </component>
</project>`);

const project = 
`<?xml version="1.0" encoding="UTF-8"?>
<module type="JAVA_MODULE" version="4">
  <component name="NewModuleRootManager" inherit-compiler-output="true">
    <exclude-output />
    <content url="file://$MODULE_DIR$">
      <sourceFolder url="file://$MODULE_DIR$/src/main/java" isTestSource="false" />
      <sourceFolder url="file://$MODULE_DIR$/src/main/resources" type="java-resource" />
      <sourceFolder url="file://$MODULE_DIR$/src/test/java" isTestSource="false" />
      <sourceFolder url="file://$MODULE_DIR$/src/test/resources" type="java-resource" />
    </content>
    <orderEntry type="inheritedJdk" />
    <orderEntry type="sourceFolder" forTests="false" />
  </component>
</module>`;

function run(projectPath, projectName) {
    const ideaTree = {
        '.idea': {
            type: 'dir',
            misc: {
                type: 'file',
                ext: 'xml',
                content: misc,
            },
            modules: {
                type: 'file',
                ext: 'xml',
                content: modules(projectName)
            },
            [projectName]: {
                type: 'file',
                ext: 'iml',
                content: project
            }
        }
    };
    createFileTree(ideaTree, path.join(projectPath, projectName));
    fs.mkdir(path.join(projectPath, projectName, 'out'), { recursive: true }, err => {
        if (err) {
            console.log("Can't create out directory");
        }
    });
}

module.exports = {
    init: run
};