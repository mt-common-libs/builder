const fs = require('fs');
const path = require('path');
const fetch = require('node-fetch');
const Promise = require('core-js/features/promise');

const differenceWith = require('lodash/differenceWith');
const isEqual = require('lodash/isEqual');
const map = require('lodash/map');
const replace = require('lodash/replace');
const forEach = require('lodash/forEach');
const concat = require('lodash/concat');
const remove = require('lodash/remove');

function run(config, pathToLibs, extensions) {
  fs.readFile(path.join(pathToLibs, 'libs.json'), (err, res) => {
    if (!err) {
      try {
        const libs = JSON.parse(res);
        const includeDiff = differenceWith(config.dependencies, libs, isEqual);
        const excludeDiff = differenceWith(libs, config.dependencies, isEqual);
        syncDependency(pathToLibs, includeDiff, excludeDiff, config.repositories, libs)
          .then(res => {
            fs.writeFile(path.join(pathToLibs, 'libs.json'), JSON.stringify(res), (err) => {
              if (err) {
                console.log("Can't update file libs.json");
              }
            });
          });
      } catch (err) {
        console.log(err);
        console.log("Can't parse libs.json file");
      }
    } else {
      console.log("Can't read libs.json file");
    }
  });

  forEach(extensions, ext => {
    ext(config, pathToLibs);
  });
}

function syncDependency(pathToLibs, include, exclude, repositories, oldLibs) {
  let newLibs = oldLibs;
  return Promise.all([
    loadDependency(pathToLibs, include, repositories)
    .then((res) => {
      forEach(res, r => {
        if (r.status === 'fulfilled') {
          console.log(`Added dependency: ${r.value.group}:${r.value.name}:${r.value.version}`);
          newLibs = concat(newLibs, r.value);
        } else {
          console.error(`Dependency ${r.reason.group}:${r.reason.name}:${r.reason.version} was not found`);
        }
      })
    }),
    clearUnnecessaryDependency(pathToLibs, exclude)
    .then((res) => {
      forEach(res, r => {
        if (r.status === 'fulfilled') {
          console.log(`Clear dependency: ${r.value.group}:${r.value.name}:${r.value.version}`);
          remove(newLibs, (l) => isEqual(r.value, l));
        } else {
          console.error(`Can't clear dependency: ${r.reason.group}:${r.reason.name}:${r.reason.version}`);
        }
      });
    })
  ])
    .then(() => Promise.resolve(newLibs));
}

function clearUnnecessaryDependency(pathToLibs, dependencies) {
  const tasks = map(dependencies, d => {
    return new Promise((res, rej) => {
      fs.unlink(path.join(pathToLibs, `${d.group}-${d.name}-${d.version}.jar`), (err) => {
        if (err) {
          rej(d);
        } else res(d);
      });
    });
  });
  return Promise.allSettled(tasks);
}

function loadDependency(pathToLibs, dependencies, repositories) {
  const tasks = map(dependencies, d => {
    const repoTasks = map(repositories, r => {
      return fetch(`${r}/${replace(d.group, /\./g, '/')}/${d.name}/${d.version}/${d.name}-${d.version}.jar`)
    });
    return Promise.any(repoTasks)
      .then(result => {
        if (result.status >= 200 && result.status <= 300) {
          return new Promise((resolve, reject) => {
            const fileStream = fs.createWriteStream(path.join(pathToLibs, `${d.group}-${d.name}-${d.version}.jar`));
            result.body.pipe(fileStream);
            result.body.on("error", (err) => {
              reject(err);
            });
            fileStream.on("finish", () => {
              resolve(d);
            });
          });
        } else return Promise.reject();
      })
      .catch(err => {
        return Promise.reject(d);
      })
  });
  return Promise.allSettled(tasks);
}

module.exports = {
  dependency: run
};