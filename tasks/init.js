const createFileTree = require('create-file-tree');
const forEach = require('lodash/forEach');

const defaultConfig = (projectName, ide) => 
(`const { repositories } = require('../builderConfig');

exports.name = "${projectName}";
exports.version = "0.0.0";

exports.ide = "${ide ? ide : "unspecified"}";

exports.repositories = [
    repositories["Maven Central"]
];

exports.dependencies = [

];
`);

const defaultMain = (projectName) => 
(`package ${projectName};

public class Main {

	public static void main(String[] args) {
		System.out.println("Hello");
	}
}`);

function run(projectPath, projectName, ide, extensions) {
    const projectTree = {
        [projectName]: {
            type: 'dir',
            libs: {
                type: 'dir',
                libs: {
                    type: 'file',
                    ext: 'json',
                    content: '[]',
                }
            },
            src: {
                type: 'dir',
                main: {
                    type: 'dir',
                    java: {
                        type: 'dir',
                        [projectName]: {
                            type: 'dir',
                            Main: {
                                type: 'file',
                                ext: 'java',
                                content: defaultMain(projectName),
                            }
                        }
                    },
                    resources: {
                        type: 'dir',
                    }
                },
                test: {
                    type: 'dir',
                    java: {
                        type: 'dir',
                        [projectName]: {
                            type: 'dir',
                        }
                    },
                    resources: {
                        type: 'dir',
                    }
                }
            },
            project: {
                type: 'file',
                ext: 'js',
                content: defaultConfig(projectName, ide),
            }
        }
    };
    createFileTree(projectTree, projectPath);
    forEach(extensions, ext => {
        ext(projectPath, projectName);
    });

    console.log(`Init empty project ${projectName} at path ${projectPath}`);
}

module.exports = { 
    init: run
};