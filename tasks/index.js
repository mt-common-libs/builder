const { dependency } = require('./dependency');
const { init } = require('./init');

module.exports = {
  dependency,
  init
};