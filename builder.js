const minimist = require('minimist');
const fs = require('fs');
const path = require('path');
const {
  init,
  dependency,
} = require('./tasks');
const extensions = require('./extensions');
const get = require('lodash/get');

const args = minimist(process.argv.slice(2));
const command = args._[0];

switch (command) {
  case 'init':
  case 'i': {
    const projectPath = get(args, 'path', get(args, 'p', ''));
    const projectName = get(args, 'name', get(args, 'n', 'sample'));
    const ide = get(args, 'ide');
    init(path.join(__dirname, projectPath), projectName, ide, findExtensions(ide, 'init'));
  } break;
  case 'dependency':
  case 'd': {
    const project = require('./testproject/sample/project');
    dependency(project, path.join(__dirname, 'testproject/sample/libs'), findExtensions(project.ide, 'dependency'));
  } break;
  default:
    console.log("No such command");
    console.log(args);
}

function findExtensions(ide, taskName) {
  const ext = [];
  if (ide && ide !== "unspecified") {
    const initExt = get(extensions, `${ide}.${taskName}`);
    if (initExt) {
      console.log(`Found ${taskName} extension for ${ide}`)
      ext.push(initExt);
    }
  }
  return ext;
}