const repositories = {
  'Maven Central': 'https://repo1.maven.org/maven2',
};

module.exports = {
  repositories,
};